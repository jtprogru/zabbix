#!/usr/bin/env bash
D=$(LC_ALL="en_US.UTF-8"  date +"%d/%b/%Y:%H:%M" -d "1 min ago")

cat /var/log/nginx/access.log | grep -i $D  | awk '{ print  $8 }' | grep -c $1

