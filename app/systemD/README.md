# systemd Service Monitoring template for Zabbix

## FEATURES
- Discovery of systemd Services
- Provides option of blacklisting or whitelisting services
- Provides alerting when a service stops or restarts

## REQUIREMENTS
- RHEL/CentOS/Oracle EL
- Ubuntu 16.04/18.04
- Zabbix 4.0

## INSTALLATION
- Server
  - Import template [template.xml](#template.xml) file
  - Link template to host
- Agent
  - Place the following files inside `/etc/zabbix/`:
      - service_discovery_blacklist or [service_discovery_whitelist](#service_discovery_whitelist)
  - Place the following file inside `/etc/zabbix/scripts/`:
    - [zbx_service_restart_check.sh](#zbx_service_restart_check.sh)
    - [zbx_service_discovery.sh](#zbx_service_discovery.sh)
  - Set executable permissions on both scripts
  - Copy [userparameter_systemd_services.conf](#userparameter_systemd.conf) to `/etc/zabbix/zabbix_agentd.d/userparameter_systemd.conf`
  - Restart zabbix_agent

## NOTES

The filter files can take extended regular expressions, one per line.
If neither `service_discovery_whitelist` nor `service_discovery_blacklist` exist on the system, the default behavior is to monitor all services. In other words it is the equivalent of a blank blacklist and a non-existent whitelist.

If both files exist, both will be used, with the whitelist filter being applied first. Their behavior is explained as follows.

Blacklist Option:
- This assumes you have disabled all unnecessary services prior to enabling the template. Any service that is enabled and not running will result in an alert.
- If you cannot, use the `service_discovery_blacklist` to add services that you don’t want to monitor.
- Additionally, this excludes getty and autovt which are not reported by `systemctl` with the tty and will result in an error.

Whitelist Option:
- I have added the whitelist option as a way allow users to select the services they wish to monitor.
- To do so modify the `service_discovery_whitelist` which is already populated with `sshd|zabbix-agent`.

## Testing
To test that everything works use `zabbix_agentd -t` to query the statistics :
```bash
# Discover systemd services
zabbix_agentd -t "systemd.service.discovery"
zabbix_agentd -t "systemd.service.status[sshd]"
zabbix_agentd -t "systemd.service.restart[sshd]"
```

